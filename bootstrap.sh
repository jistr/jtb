#!/bin/bash

set -euo pipefail
set -x

DIR=$(dirname $(realpath "$0"))

FROM=quay.io/fedora/fedora:40-$(uname -m)
BUILDAH="buildah "
PODMAN="podman "
BR="$BUILDAH run jtb-bootstrap -- "
BRI="$BUILDAH run jtb-bootstrap -- dnf -y install "

# remote => pull jtb-shell image from Quay
# local => build jtb-bootstrap image locally
BOOTSTRAP_IMAGE_SOURCE=${BOOTSTRAP_IMAGE_SOURCE:-remote}

UID_MAX=65536
UID_HIGHER_START=$(($UID+1))
UID_HIGHER_COUNT=$((UID_MAX-$UID))
[ -e /usr/lib/passwd ] && USR_LIB_PASSWD="-v /usr/lib/passwd:/usr/lib/passwd" || USR_LIB_PASSWD=""
[ -e /usr/lib/group ] && USR_LIB_GROUP="-v /usr/lib/group:/usr/lib/group" || USR_LIB_GROUP=""
[ -e /var/home ] && VAR_HOME="-v /var/home:/var/home" || VAR_HOME=""

if [ "$BOOTSTRAP_IMAGE_SOURCE" = "remote" ]; then
    BOOTSTRAP_IMAGE=quay.io/jistr/jtb-shell
    podman pull "$BOOTSTRAP_IMAGE"
elif [ "$BOOTSTRAP_IMAGE_SOURCE" = "local" ]; then
    BOOTSTRAP_IMAGE="localhost/jtb-bootstrap"
    if ! $PODMAN inspect "$BOOTSTRAP_IMAGE" &> /dev/null; then
        $BUILDAH from --name jtb-bootstrap $FROM
        $BR dnf -y update
        $BR dnf -y remove --noautoremove xdg-utils
        $BRI ansible dbus-x11 flatpak-xdg-utils make nss-altfiles
        $BR bash -c 'echo "#!/bin/bash" > /usr/local/bin/buildah'
        $BR bash -c 'echo "flatpak-spawn --host buildah \"\$@\"" >> /usr/local/bin/buildah'
        $BR bash -c 'chmod 0755 /usr/local/bin/buildah'
        $BR bash -c 'echo "#!/bin/bash" > /usr/local/bin/podman'
        $BR bash -c 'echo "flatpak-spawn --host podman \"\$@\"" >> /usr/local/bin/podman'
        $BR bash -c 'chmod 0755 /usr/local/bin/podman'
        $BUILDAH commit jtb-bootstrap "$BOOTSTRAP_IMAGE"
        $BUILDAH rm jtb-bootstrap
    fi
else
    echo 'Incorrect $BOOTSTRAP_IMAGE_SOURCE.'
fi

PODMAN_RUN_SNIP="
$PODMAN run \
    -e JTB_ROOT_PATH=$DIR \
    -e JTB_BUILD_ARGS \
    -e JTB_SHELL_BUILD_ARGS \
    \
    -e DBUS_SESSION_BUS_ADDRESS \
    -e DISPLAY \
    -e WAYLAND_DISPLAY \
    -e XDG_RUNTIME_DIR \
    -v $XDG_RUNTIME_DIR:$XDG_RUNTIME_DIR \
    \
    -v /etc/subuid:/etc/subuid:ro \
    -v /etc/subgid:/etc/subgid:ro \
    -v /etc/passwd:/etc/passwd:ro \
    -v /etc/group:/etc/group:ro \
    -v /etc/nsswitch.conf:/etc/nsswitch.conf:ro \
    -v /etc/security/limits.d:/etc/security/limits.d:ro \
    $USR_LIB_PASSWD \
    $USR_LIB_GROUP \
    \
    --uidmap $UID:0:1 \
    --uidmap 0:1:$UID \
    --uidmap $UID_HIGHER_START:$UID_HIGHER_START:$UID_HIGHER_COUNT \
    --gidmap $UID:0:1 \
    --gidmap 0:1:$UID \
    --gidmap $UID_HIGHER_START:$UID_HIGHER_START:$UID_HIGHER_COUNT \
    \
    -v $HOME:$HOME \
    $VAR_HOME \
    --privileged \
    --rm \
    --net host \
    --pid host \
    --security-opt label=disable \
    --volume /var/run/dbus/system_bus_socket:/var/run/dbus/system_bus_socket \
    --volume /dev/dri:/dev/dri \
    --volume /dev/fuse:/dev/fuse \
    -ti"

PODMAN_RUN_USER="
    $PODMAN_RUN_SNIP \
    --user $UID:$UID \
    $BOOTSTRAP_IMAGE"

if $PODMAN inspect quay.io/jistr/jtb-shell &> /dev/null; then
    $PODMAN_RUN_USER bash -c 'cd $JTB_ROOT_PATH; export JTB_BUILD_ARGS; export JTB_SHELL_BUILD_ARGS; make build-bin-shell'
else
    $PODMAN_RUN_USER bash -c 'cd $JTB_ROOT_PATH; export JTB_BUILD_ARGS; export JTB_SHELL_BUILD_ARGS; make build-shell'
fi

$PODMAN rmi jtb-bootstrap || true
