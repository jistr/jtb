jtb - jistr's toolbox
=====================

Build scripts for container images with various tools and
apps. Intended for usage with Fedora Silverblue.

Important notes:

* There is an official
  [toolbox](https://github.com/debarshiray/toolbox) in Fedora
  Silverblue. If you haven't explored it yet, you should give it a
  look as chances are you'll be better served by toolbox than jtb.

  * Jtb uses pre-built images approach, toolbox starts a minimal
    container for you to work with.

  * Toolbox is more beginner-friendly, easy to use, and generic. Jtb
    aims to bring more repeatability and automation. However, jtb only
    targets specific use cases and requires willingness and ability to
    dive into details in order to achieve efficient use.

* Apart from the "developer shell" use case, jtb also creates
  container images for some apps i use. These are mainly to substitute
  for missing flatpaks, so it's possible that these images will be
  removed from jtb if/when suitable flatpak replacements are created.

* Currently jtb is focused around my personal use cases. I'm not
  convinced it is realistic to cover use cases of a wider community
  when using the pre-built images approach. If you like the jtb
  approach in general but are missing something, it may be best to use
  jtb as a starting point -- fork it and create your own personal
  toolbox.


Bootstrapping
-------------

Clone the repository into `~/.jtb`:

    git clone https://gitlab.com/jistr/jtb ~/.jtb

Add the jtb bin directory at the end of your `$PATH` in files like
`profile` and `bash_profile`:

    export PATH="$PATH:$HOME/.jtb/build/bin"

When building on a fresh Silverblue, lack of Make and Ansible presents
a chicken-and-egg problem for building the container images. Use the
bootstrap script to build the first `jtb-shell` image:

    ./bootstrap.sh

Now that jtb-shell image and script are present, run the script to
enter a shell container:

    jtb-shell

All `make` commands in following sections should be run from the
jtb-shell container to make sure you have the necessary commands
available (`make`, `ansible-playbook` etc.).


Building
--------

Images can be built either in default set, or indiviual ones, or all.

    make build        # build default set
    make build-shell  # build individual image (shell)
    make build-all    # build all

After an image is built, you'll find commands which use that image in
`$HOME/.jtb/build/bin`.


Updating
--------

Jtb leaves Buildah containers present to speed up image updates. To
update RPMs in the default set of images, use the `update-nodeps` make
target.

    make update-nodeps        # update default image set
    make update-nodeps-shell  # update an individual image (shell)

Updating in this way can make the Buildah containers grow in size over
time. The images cannot be rebased, they are just updated
within. (This is also why there's `nodeps` in update -- it doesn't
matter in what order the images are updated or whether base images are
updated too or not. Updating always happens "on leaf level".) To not
have container images grow indefinitely, it is recommended to
occasionally `rebuild` instead of updating.


Rebuilding
----------

To rebuild the default set of images:

    make rebuild

To rebuild a single image and its dependencies:

    make rebuild-shell

Note if you're rebuilding a single image, you may not want to rebuild
its deps too. E.g. if you rebuild `base` as part of rebulding `shell`,
any other images will still be based on the older version of
`base`. It may be better to rebuild just the leaf image without
dependencies:

    make rebuild-nodeps-shell


Cleaning up
-----------

Jtb tags images by date when they've been built. This will allow you
to get back to a known good image should any issues appear with the
latest image. It's recommended to expire old images to free up space,
which will keep a maximum of two most recent builds per each image:

    make expire-all

You can also combine e.g. updating and expiring into one command:

    make rebuild expire-all


License
-------

GNU GPL v3 or later
