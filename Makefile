.SECONDEXPANSION:
.PHONY: build clean
.DEFAULT_GOAL := build

KEEP_IMAGES ?= 2
JTB_BUILD_SKIP_TAGS ?= --skip-tags jtb_clean
JTB_IMAGE_ORG ?= quay.io/jistr

# should be in reverse-dependency order where applicable, to allow
# generating clean and expire rules easily
ALL_IMAGES = audyoyo firefox-codecs firefox mpd shell base
DEFAULT_IMAGES = firefox-codecs firefox mpd shell base

# only default images
build: $(addprefix build-,$(DEFAULT_IMAGES))
build-bin: $(addprefix build-bin-,$(DEFAULT_IMAGES))
update-nodeps: $(addprefix update-nodeps-,$(DEFAULT_IMAGES))
clean: $(addprefix clean-,$(DEFAULT_IMAGES))
push: push-base $(addprefix push-,$(DEFAULT_IMAGES))
pull: $(addprefix pull-,$(DEFAULT_IMAGES))
rebuild: $(addprefix rebuild-,$(DEFAULT_IMAGES))

build-all: $(addprefix build-,$(ALL_IMAGES))
build-bin-all: $(addprefix build-bin-,$(ALL_IMAGES))
clean-all: $(addprefix clean-,$(ALL_IMAGES))
	rm -rf ./build
rebuild-all: $(addprefix rebuild-,$(ALL_IMAGES))
expire-all: $(addprefix expire-,$(ALL_IMAGES))
push-all: $(addprefix push-,$(ALL_IMAGES))
pull-all: $(addprefix pull-,$(ALL_IMAGES))

define IMAGE_RULES
build-$(1): $(addprefix build-,$(2)) build-nodeps-$(1)

build-nodeps-$(1):
	cd ansible; ansible-playbook -v \
		-i localhost \
		-e jtb_image_update=$$(JTB_UPDATE) \
		-e jtb_image_rebuild=$$(JTB_REBUILD) \
		$(JTB_BUILD_SKIP_TAGS) \
		$(JTB_BUILD_ARGS) \
		$(JTB_$(shell echo $(1) | tr a-z A-Z)_BUILD_ARGS) \
		playbooks/$(1).yml

build-bin-$(1): $(addprefix build-bin-,$(2))
	cd ansible; ansible-playbook -v \
		-i localhost \
		--tags jtb_bin \
		$(JTB_BUILD_SKIP_TAGS) \
		$(JTB_BUILD_ARGS) \
		$(JTB_$(shell echo $(1) | tr a-z A-Z)_BUILD_ARGS) \
		playbooks/$(1).yml

clean-$(1):
	buildah rm jtb-$(1)-build || true

rebuild-$(1): JTB_REBUILD=true
rebuild-$(1): build-$(1)

rebuild-nodeps-$(1): JTB_REBUILD=true
rebuild-nodeps-$(1): build-nodeps-$(1)

update-nodeps-$(1): JTB_UPDATE=true
update-nodeps-$(1): build-nodeps-$(1)

expire-$(1):
	TAGS=$$$$(podman images | grep '^$(JTB_IMAGE_ORG)/jtb-$(1) ' | grep -E ' [0-9]{4}_[0-9]{2}_[0-9]{2} ' | awk '{ print $$$$2;}' | sort -r); \
	IDX=0 ; \
	for TAG in $$$$TAGS; do \
		if [ "$$$$IDX" -ge "$(KEEP_IMAGES)" ]; then \
			podman rmi $(JTB_IMAGE_ORG)/jtb-$(1):$$$$TAG; \
		fi; \
		: $$$$(( IDX++ )); \
	done

push-$(1):
	podman push $(JTB_IMAGE_ORG)/jtb-$(1):latest

pull-$(1): build-bin-$(1)
	podman pull $(JTB_IMAGE_ORG)/jtb-$(1):latest; \
	podman tag $(JTB_IMAGE_ORG)/jtb-$(1):latest $(JTB_IMAGE_ORG)/jtb-$(1):$(shell date '+%Y_%m_%d')
endef

$(eval $(call IMAGE_RULES,audyoyo,base))
$(eval $(call IMAGE_RULES,base,))
$(eval $(call IMAGE_RULES,mpd,shell))
$(eval $(call IMAGE_RULES,firefox,base))
$(eval $(call IMAGE_RULES,firefox-codecs,firefox))
$(eval $(call IMAGE_RULES,shell,base))

build/images:
	mkdir -p $@

build/images/%.tar: | build/images
	podman save --format oci-archive -o $@ $(JTB_IMAGE_ORG)/$(notdir $(basename $@)):latest
